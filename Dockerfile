FROM tomcat:9-jdk17-openjdk-slim

RUN rm -rf /usr/local/tomcat/webapps/*

COPY ./build/libs/urlshortener-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war
COPY ./src/main/resources/application.yml /usr/local/tomcat/webapps/application.yml

EXPOSE 8080

CMD ["catalina.sh","run"]