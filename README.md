# Url-shortener
Backend to generate and manage short urls

## Git Hooks
In the pre-commit hook, it will run ktlintcheck in the project to verify the rules for Kotlin language (rules by default).

To verify errors run: `./gradlew ktlintcheck`  
To format code run: `./gradlew ktlintformat`

## Docker compose setup
This project use docker to an easy deploy, and docker-compose for an easy develop experience too.  

#### DB
If you want to deploy the database, you can run this command: `docker compose up db` this use the default port for 
postgres (5432), the username is *url-shortener-user* and the password is *url-shortener-passwd* if you want 
to use a client like **pg-admin** or **dbeaver**.

### App
To run the app you will need first have running the db, if you have, run `docker compose up url-shortener`.

To run the whole project just run: `docker compose up`. It will be deployed in the port 8080, 
go to your [localhost:8080](http://localhost:8080).

## Api docs
To have a better experience to use this api, I'm using swagger, once you have runnig the app, you can 
go to your [localhost:8080/swagger](http://localhost:8080/swagger-ui/index.html).
