package com.rvladimir.urlshortener.web.rest

import com.rvladimir.urlshortener.domain.Url
import com.rvladimir.urlshortener.domain.User
import com.rvladimir.urlshortener.repository.UrlRepository
import com.rvladimir.urlshortener.repository.UserRepository
import com.rvladimir.urlshortener.service.dto.LoginDTO
import com.rvladimir.urlshortener.service.mapper.UrlMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest()
class UrlResourceTest {

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var urlRepository: UrlRepository

    @Autowired
    private lateinit var urlMapper: UrlMapper

    @Autowired
    private lateinit var restMockMvc: MockMvc

    private lateinit var user: User

    private lateinit var url: Url

    private lateinit var token: String

    @BeforeEach
    fun setUp() {
        user = createUserEntity()
        url = createUrlEntity()
        token = getTokenFromUser()
    }

    @AfterEach
    fun cleanSetUp() {
        urlRepository.deleteAll()
        userRepository.deleteAll()
    }

    fun getTokenFromUser(): String {
        user.password = BCryptPasswordEncoder().encode(user.password)
        userRepository.saveAndFlush(user)

        val loginDTO = LoginDTO(DEFAULT_USER_EMAIL, DEFAULT_USER_PASSWORD)

        val mvcResult: MvcResult = restMockMvc.perform(
            post("$ENTITY_USER_API_URL/login").contentType(MediaType.APPLICATION_JSON_VALUE).content(
                convertObjectToJsonBytes(loginDTO)
            )
        ).andReturn()

        return mvcResult.response.contentAsString.split(":")[1].replace("\"", "").replace("}", "")
    }

    @Test
    fun `should create a new url`() {
        val userFromDb = userRepository.findByEmail(DEFAULT_USER_EMAIL)
        if (userFromDb != null) {
            url.user = userFromDb
        }
        val urlDTO = urlMapper.toDto(url)
        val databaseSizeBeforeCreate = urlRepository.findAll().size

        restMockMvc.perform(
            post("$ENTITY_URL_API_URL/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization", "Bearer $token").content(
                    convertObjectToJsonBytes(urlDTO)
                )
        ).andExpect(status().isCreated)

        val urlList = urlRepository.findAll()
        assertThat(urlList).hasSize(databaseSizeBeforeCreate + 1)

        val testUrl = urlList[urlList.size - 1]
        assertThat(testUrl.name).isEqualTo(DEFAULT_URL_NAME)
        assertThat(testUrl.link).isEqualTo(DEFAULT_URL_LINK)
        assertThat(testUrl.hash).isEqualTo(DEFAULT_URL_HASH)
        assertThat(testUrl.clicks).isEqualTo(DEFAULT_URL_CLICKS)
        assertThat(testUrl.creationDate).isNotNull()
    }

    @Test
    fun `should return a page of urls`() {
        val userFromDb = userRepository.findByEmail(DEFAULT_USER_EMAIL)
        if (userFromDb != null) {
            url.user = userFromDb
        }
        urlRepository.saveAndFlush(url)

        restMockMvc.perform(
            get("$ENTITY_URL_API_URL/urls").param("page", "0").param("size", "2")
                .contentType(MediaType.APPLICATION_JSON_VALUE).header("Authorization", "Bearer $token")
        ).andExpect(status().is2xxSuccessful).andExpect(jsonPath("$.totalPages").exists())
            .andExpect(jsonPath("$.totalPages").value(1)).andExpect(jsonPath("$.totalElements").exists())
            .andExpect(jsonPath("$.totalElements").value(1)).andExpect(jsonPath("$.numberOfElements").exists())
            .andExpect(jsonPath("$.numberOfElements").value(1))
    }

    companion object {

        private const val DEFAULT_USER_NAME = "VLADIMIR"

        private const val DEFAULT_USER_LAST_NAME = "VACA"

        private const val DEFAULT_USER_EMAIL = "VLADIMIRVACA@GMAIL.COM"

        private const val DEFAULT_USER_PASSWORD = "VLADIMIR1234"

        private const val DEFAULT_URL_NAME = "TWITTER"

        private const val DEFAULT_URL_LINK = "https://twitter.com"

        private const val DEFAULT_URL_HASH = "twtt"

        private const val DEFAULT_URL_CLICKS = 0

        private const val ENTITY_USER_API_URL: String = "/api/user"

        private const val ENTITY_URL_API_URL: String = "/api/url"

        @JvmStatic
        fun createUserEntity(): User {
            return User(
                name = DEFAULT_USER_NAME,
                lastname = DEFAULT_USER_LAST_NAME,
                email = DEFAULT_USER_EMAIL,
                password = DEFAULT_USER_PASSWORD
            )
        }

        @JvmStatic
        fun createUrlEntity(): Url {
            return Url(
                name = DEFAULT_URL_NAME,
                link = DEFAULT_URL_LINK,
                hash = DEFAULT_URL_HASH
            )
        }

        @Container
        private val postgreSQLContainer = PostgreSQLContainer<Nothing>("postgres:15.3")

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl)
            registry.add("spring.datasource.username", postgreSQLContainer::getUsername)
            registry.add("spring.datasource.password", postgreSQLContainer::getPassword)
        }
    }
}
