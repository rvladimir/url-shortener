package com.rvladimir.urlshortener.web.rest

import com.rvladimir.urlshortener.domain.User
import com.rvladimir.urlshortener.repository.UserRepository
import com.rvladimir.urlshortener.service.dto.LoginDTO
import com.rvladimir.urlshortener.service.mapper.UserMapper
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest()
class UserResourceTest {

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var userMapper: UserMapper

    @Autowired
    private lateinit var restMockMvc: MockMvc

    private lateinit var user: User

    @BeforeEach
    fun setUp() {
        user = createEntity()
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun `should create a user for first time`() {
        val databaseSizeBeforeCreate = userRepository.findAll().size
        val userDTO = userMapper.toDto(user)

        restMockMvc.perform(
            post("$ENTITY_API_URL/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(convertObjectToJsonBytes(userDTO))
        ).andExpect(status().isCreated)

        val userList = userRepository.findAll()
        assertThat(userList).hasSize(databaseSizeBeforeCreate + 1)

        val testUser = userList[userList.size - 1]

        assertThat(testUser.name).isEqualTo(DEFAULT_NAME)
        assertThat(testUser.lastname).isEqualTo(DEFAULT_LAST_NAME)
        assertThat(testUser.email).isEqualTo(DEFAULT_EMAIL)
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun `should not allow create another user if one already exist`() {
        userRepository.saveAndFlush(user)
        val userDTO = userMapper.toDto(user)

        restMockMvc.perform(
            post("$ENTITY_API_URL/create").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(convertObjectToJsonBytes(userDTO))
        ).andExpect(status().is4xxClientError)
            .andExpect(content().string("A user already exists, we can't allow more ..."))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun `should login a user`() {
        user.password = BCryptPasswordEncoder().encode(user.password)
        userRepository.saveAndFlush(user)
        val loginDTO = LoginDTO(DEFAULT_EMAIL, DEFAULT_PASSWORD)

        restMockMvc.perform(
            post("$ENTITY_API_URL/login").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(convertObjectToJsonBytes(loginDTO))
        ).andExpect(status().is2xxSuccessful).andExpect(content().string(Matchers.containsString("\"token\":")))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun `should fail login with incorrect email`() {
        userRepository.saveAndFlush(user)
        val loginDTO = LoginDTO(DEFAULT_EMAIL_WRONG, DEFAULT_PASSWORD)

        restMockMvc.perform(
            post("$ENTITY_API_URL/login").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(convertObjectToJsonBytes(loginDTO))
        ).andExpect(status().is4xxClientError).andExpect(content().string("Invalid credentials."))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun `should fail login with non-exist email`() {
        val loginDTO = LoginDTO(DEFAULT_EMAIL, DEFAULT_PASSWORD)

        restMockMvc.perform(
            post("$ENTITY_API_URL/login").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(convertObjectToJsonBytes(loginDTO))
        ).andExpect(status().is4xxClientError).andExpect(content().string("Invalid credentials."))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun `should fail login with incorrect password`() {
        userRepository.saveAndFlush(user)
        val loginDTO = LoginDTO(DEFAULT_EMAIL, DEFAULT_PASSWORD_WRONG)

        restMockMvc.perform(
            post("$ENTITY_API_URL/login").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(convertObjectToJsonBytes(loginDTO))
        ).andExpect(status().is4xxClientError).andExpect(content().string("Invalid credentials."))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun `should return user status check false`() {
        restMockMvc.perform(
            post("$ENTITY_API_URL/check").contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().is2xxSuccessful).andExpect(content().string("{\"created\":false}"))
    }

    @Test
    @Transactional
    @Throws(Exception::class)
    fun `should return user status check true`() {
        userRepository.saveAndFlush(user)
        restMockMvc.perform(
            post("$ENTITY_API_URL/check").contentType(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().is2xxSuccessful).andExpect(content().string("{\"created\":true}"))
    }

    companion object {

        private const val DEFAULT_NAME = "VLADIMIR"

        private const val DEFAULT_LAST_NAME = "VACA"

        private const val DEFAULT_EMAIL = "VLADIMIRVACA@GMAIL.COM"

        private const val DEFAULT_EMAIL_WRONG = "${DEFAULT_EMAIL}WRONG"

        private const val DEFAULT_PASSWORD = "VLADIMIR1234"

        private const val DEFAULT_PASSWORD_WRONG = "${DEFAULT_PASSWORD}WRONG"

        private const val ENTITY_API_URL: String = "/api/user"

        @JvmStatic
        fun createEntity(): User {
            return User(
                name = DEFAULT_NAME,
                lastname = DEFAULT_LAST_NAME,
                email = DEFAULT_EMAIL,
                password = DEFAULT_PASSWORD
            )
        }

        @Container
        private val postgreSQLContainer = PostgreSQLContainer<Nothing>("postgres:15.3")

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl)
            registry.add("spring.datasource.username", postgreSQLContainer::getUsername)
            registry.add("spring.datasource.password", postgreSQLContainer::getPassword)
        }
    }
}
