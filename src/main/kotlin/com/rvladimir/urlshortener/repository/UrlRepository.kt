package com.rvladimir.urlshortener.repository

import com.rvladimir.urlshortener.domain.Url
import org.springframework.data.jpa.repository.JpaRepository

interface UrlRepository : JpaRepository<Url, Long>
