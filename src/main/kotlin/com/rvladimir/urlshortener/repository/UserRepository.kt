package com.rvladimir.urlshortener.repository

import com.rvladimir.urlshortener.domain.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Long> {
    fun findByEmail(email: String): User?
}
