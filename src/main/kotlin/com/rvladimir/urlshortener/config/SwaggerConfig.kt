package com.rvladimir.urlshortener.config

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.security.SecurityScheme
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@SecurityScheme(
    name = "Bearer Authentication",
    type = SecuritySchemeType.HTTP,
    bearerFormat = "JWT",
    scheme = "bearer"
)
class SwaggerConfig {
    @Bean
    fun customOpenAPI(): OpenAPI {
        return OpenAPI().components(Components()).info(
            Info()
                .title("Url Shortener")
                .description("This is the API for the url shortener. Created by rvladimir with ❤\uFE0F")
                .contact(
                    Contact().name("Vladimir Vaca").email("ramvlay@gmail.com").url("https://www.blog.rvladimir.com")
                )
        )
    }
}
