package com.rvladimir.urlshortener.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class ApplicationProperties {

    @Value("\${spring.application.name}")
    lateinit var applicationName: String

    @Value("\${security.authentication.jwt.base64-secret}")
    lateinit var base64Secret: String

    @Value("\${security.authentication.jwt.token-validity-in-seconds}")
    lateinit var tokenValidityInSeconds: String

    @Value("\${security.authentication.jwt.token-validity-in-seconds-for-remember-me}")
    lateinit var tokenValidityInSecondsForRememberMe: String
}
