package com.rvladimir.urlshortener.web.rest

import com.rvladimir.urlshortener.security.TokenProvider
import com.rvladimir.urlshortener.service.UserService
import com.rvladimir.urlshortener.service.dto.AuthenticationResponseDTO
import com.rvladimir.urlshortener.service.dto.LoginDTO
import com.rvladimir.urlshortener.service.dto.UserDTO
import com.rvladimir.urlshortener.service.dto.UserStatusCheckDTO
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URI

@RestController
@Tag(name = "User Resource")
@RequestMapping("/api/user")
class UserResource(
    private val userService: UserService,
    private val tokenProvider: TokenProvider,
    private val passwordEncoder: PasswordEncoder,
    private val authenticationManager: AuthenticationManager
) {
    private val log = LoggerFactory.getLogger(javaClass)

    @PostMapping("/create")
    @ApiResponse(responseCode = "201", description = "User created successfully.")
    @ApiResponse(responseCode = "400", description = "A user already exists, we can't allow more ...")
    fun createUser(
        @RequestBody userDTO: UserDTO
    ): ResponseEntity<String> {
        log.debug("REST request to create User: {}", userDTO.email)

        if (userService.findAll().isEmpty()) {
            val result = userService.create(userDTO)

            return ResponseEntity.created(URI("/api/user/${result.id}")).body("User created successfully")
        }

        return ResponseEntity.badRequest().body("A user already exists, we can't allow more ...")
    }

    @PostMapping("/login")
    @ApiResponse(responseCode = "200", description = "Token generated successfully.")
    @ApiResponse(responseCode = "400", description = "Validation error or missing field.")
    @ApiResponse(responseCode = "401", description = "Invalid credentials.")
    fun loginUser(@RequestBody loginDto: LoginDTO): ResponseEntity<*> {
        log.debug("REST request to login User: {}", loginDto.email)

        val user = userService.findByEmail(loginDto.email)

        if (user == null || !passwordEncoder.matches(loginDto.password, user.password)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid credentials.")
        }

        val authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                loginDto.email,
                loginDto.password
            )
        )
        val token = tokenProvider.createToken(authentication, user.id!!, false)

        return ResponseEntity.ok(AuthenticationResponseDTO(token))
    }

    @PostMapping("/check")
    @ApiResponse(responseCode = "200", description = "User already created or not.")
    fun checkUser(): ResponseEntity<*> {
        log.debug("REST request to check if User exist")

        if (userService.findAll().isNotEmpty()) {
            return ResponseEntity.ok(UserStatusCheckDTO(true))
        }
        return ResponseEntity.ok(UserStatusCheckDTO(false))
    }
}
