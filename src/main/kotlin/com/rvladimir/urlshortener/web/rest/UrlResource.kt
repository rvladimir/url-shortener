package com.rvladimir.urlshortener.web.rest

import com.rvladimir.urlshortener.service.UrlService
import com.rvladimir.urlshortener.service.dto.UrlDTO
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import org.slf4j.LoggerFactory
import org.springdoc.api.annotations.ParameterObject
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URI

@RestController
@Tag(name = "Url Resource")
@RequestMapping("/api/url")
class UrlResource(
    private val urlService: UrlService
) {
    private val log = LoggerFactory.getLogger(javaClass)

    @PostMapping("/create")
    @SecurityRequirement(name = "Bearer Authentication")
    @ApiResponse(responseCode = "200", description = "Url created successfully")
    fun createUrl(@RequestBody urlDTO: UrlDTO): ResponseEntity<String> {
        log.debug("Rest request to create a Url: {}", urlDTO.name)

        val result = urlService.create(urlDTO)

        return ResponseEntity.created((URI("/api/url/${result.id}"))).body(result.toString())
    }

    @GetMapping("/urls")
    @SecurityRequirement(name = "Bearer Authentication")
    @ApiResponse(responseCode = "200", description = "Page with url's")
    fun getAllUrls(@ParameterObject pageable: Pageable): ResponseEntity<Page<UrlDTO>> {
        log.debug(
            "Rest request to get a Page with Url's: page {}, page size {}",
            pageable.pageNumber,
            pageable.pageSize
        )
        val page = urlService.findAll(pageable)
        return ResponseEntity.ok().body(page)
    }
}
