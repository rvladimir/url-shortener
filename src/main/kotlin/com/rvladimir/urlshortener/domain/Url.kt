package com.rvladimir.urlshortener.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.CreationTimestamp
import java.io.Serializable
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "url")
data class Url(

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    var id: Long? = null,

    @Column(unique = true, nullable = false)
    var name: String? = null,

    @Column(unique = true, nullable = false)
    var link: String? = null,

    @Column(unique = true)
    var hash: String? = null,

    @Column(columnDefinition = "integer default 0")
    var clicks: Int? = 0,

    @CreationTimestamp
    var creationDate: Date? = null
) : Serializable {

    @ManyToOne
    @JsonIgnoreProperties(
        value = [
            "urls"
        ],
        allowSetters = true
    )
    lateinit var user: User

    fun user(user: User): Url {
        this.user = user
        return this
    }
}
