package com.rvladimir.urlshortener.security

import com.rvladimir.urlshortener.config.ApplicationProperties
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.JwtParser
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.UnsupportedJwtException
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import io.jsonwebtoken.security.SignatureException
import net.minidev.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component
import org.springframework.util.ObjectUtils
import java.nio.charset.StandardCharsets
import java.security.Key
import java.util.Date

private const val AUTHORITIES_KEY = "auth"
private const val INVALID_JWT_TOKEN = "Invalid JWT token."

@Component
class TokenProvider(applicationProperties: ApplicationProperties) {

    private val log = LoggerFactory.getLogger(javaClass)

    private val key: Key = run {
        val base64Secret = applicationProperties.base64Secret

        val keyBytes: ByteArray = if (!ObjectUtils.isEmpty(base64Secret)) {
            log.debug("Using a Base64-encoded JWT secret key")
            Decoders.BASE64.decode(base64Secret)
        } else {
            log.warn(
                "Warning: the JWT key used is not Base64-encoded. "
            )
            val secret = "mysupersecretkeymysupersecretkeymysupersecretkeymysupersecretkeymysupersecretkey"
            secret.toByteArray(StandardCharsets.UTF_8)
        }

        Keys.hmacShaKeyFor(keyBytes)
    }

    private val jwtParser: JwtParser = Jwts.parserBuilder().setSigningKey(key).build()

    private val tokenValidityInMilliseconds: Long =
        1000 * applicationProperties.tokenValidityInSeconds.toLong()

    private val tokenValidityInMillisecondsForRememberMe: Long =
        1000 * applicationProperties.tokenValidityInSecondsForRememberMe.toLong()

    fun createToken(authentication: Authentication, userId: Long, rememberMe: Boolean): String {
        val authorities = authentication.authorities.asSequence()
            .map { it.authority }
            .joinToString(separator = ",")

        val now = Date().time
        val validity = if (rememberMe) {
            Date(now + this.tokenValidityInMillisecondsForRememberMe)
        } else {
            Date(now + this.tokenValidityInMilliseconds)
        }

        val jsonPayload = JSONObject()
        jsonPayload["userEmail"] = authentication.name
        jsonPayload["userId"] = userId

        return Jwts.builder()
            .setSubject(jsonPayload.toJSONString())
            .claim(AUTHORITIES_KEY, authorities)
            .signWith(key, SignatureAlgorithm.HS512)
            .setExpiration(validity)
            .compact()
    }

    fun getAuthentication(token: String): Authentication {
        val claims = jwtParser.parseClaimsJws(token)?.body

        val authorities = claims?.get(AUTHORITIES_KEY)?.toString()
            ?.splitToSequence(",")
            ?.filter { it.trim().isNotEmpty() }
            ?.mapTo(mutableListOf()) { SimpleGrantedAuthority(it) }

        val principal = User(claims?.subject, "", authorities)

        return UsernamePasswordAuthenticationToken(principal, token, authorities)
    }

    fun validateToken(authToken: String): Boolean {
        try {
            jwtParser.parseClaimsJws(authToken)
            return true
        } catch (e: ExpiredJwtException) {
            log.trace(INVALID_JWT_TOKEN, e)
        } catch (e: UnsupportedJwtException) {
            log.trace(INVALID_JWT_TOKEN, e)
        } catch (e: MalformedJwtException) {
            log.trace(INVALID_JWT_TOKEN, e)
        } catch (e: SignatureException) {
            log.trace(INVALID_JWT_TOKEN, e)
        } catch (e: IllegalArgumentException) {
            log.error("Token validation error {}", e.message)
        }

        return false
    }
}
