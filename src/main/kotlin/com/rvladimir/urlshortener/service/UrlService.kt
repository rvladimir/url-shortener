package com.rvladimir.urlshortener.service

import com.rvladimir.urlshortener.service.dto.UrlDTO
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface UrlService {
    fun create(urlDTO: UrlDTO): UrlDTO
    fun findAll(pageable: Pageable): Page<UrlDTO>
}
