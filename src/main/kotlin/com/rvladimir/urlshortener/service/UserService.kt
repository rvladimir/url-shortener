package com.rvladimir.urlshortener.service

import com.rvladimir.urlshortener.domain.User
import com.rvladimir.urlshortener.service.dto.UserDTO
import org.springframework.security.core.userdetails.UserDetailsService

interface UserService : UserDetailsService {
    fun create(userDTO: UserDTO): UserDTO
    fun findByEmail(email: String): User?
    fun findAll(): List<User>
}
