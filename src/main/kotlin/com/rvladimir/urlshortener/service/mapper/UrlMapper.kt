package com.rvladimir.urlshortener.service.mapper

import com.rvladimir.urlshortener.domain.Url
import com.rvladimir.urlshortener.domain.User
import com.rvladimir.urlshortener.service.dto.UrlDTO
import com.rvladimir.urlshortener.service.dto.UserDTO
import org.mapstruct.BeanMapping
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.Named

/**
 * Mapper for the entity [Url] and its DTO [UrlDTO].
 */
@Mapper(componentModel = "spring")
interface UrlMapper : EntityMapper<UrlDTO, Url> {

    @Mappings(
        Mapping(target = "user", source = "user", qualifiedByName = ["userId"])
    )
    override fun toDto(entity: Url): UrlDTO

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mappings(
        Mapping(target = "id", source = "id")
    )
    fun toDtoUserId(user: User): UserDTO
}
