package com.rvladimir.urlshortener.service.mapper

import com.rvladimir.urlshortener.domain.User
import com.rvladimir.urlshortener.service.dto.UserDTO
import org.mapstruct.Mapper

/**
 * Mapper for the entity [User] and its DTO [UserDTO].
 */
@Mapper(componentModel = "spring")
interface UserMapper : EntityMapper<UserDTO, User>
