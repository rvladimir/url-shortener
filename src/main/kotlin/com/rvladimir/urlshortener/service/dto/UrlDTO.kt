package com.rvladimir.urlshortener.service.dto

import java.io.Serializable
import java.util.*

data class UrlDTO(
    var id: Long? = null,

    var name: String? = null,

    var link: String? = null,

    var hash: String? = null,

    var user: UserDTO? = null,

    var clicks: Int? = null,

    var creationDate: Date? = null
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is UrlDTO) return false
        if (this.id == null) {
            return false
        }
        return Objects.equals(this.id, other.id)
    }

    override fun hashCode() = Objects.hash(this.id)
}
