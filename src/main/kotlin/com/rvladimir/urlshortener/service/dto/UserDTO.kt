package com.rvladimir.urlshortener.service.dto

import java.io.Serializable
import java.util.Objects

data class UserDTO(
    var id: Long? = null,

    var name: String? = null,

    var lastname: String? = null,

    var email: String? = null,

    var password: String? = null,

    var urls: List<UrlDTO>? = null
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is UserDTO) return false
        if (this.id == null) {
            return false
        }
        return Objects.equals(this.id, other.id)
    }

    override fun hashCode() = Objects.hash(this.id)
}
