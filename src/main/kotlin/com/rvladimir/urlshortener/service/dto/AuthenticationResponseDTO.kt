package com.rvladimir.urlshortener.service.dto

data class AuthenticationResponseDTO(val token: String)
