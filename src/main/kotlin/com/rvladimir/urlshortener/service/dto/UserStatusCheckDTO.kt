package com.rvladimir.urlshortener.service.dto

data class UserStatusCheckDTO(
    val created: Boolean
)
