package com.rvladimir.urlshortener.service.impl

import com.rvladimir.urlshortener.domain.User
import com.rvladimir.urlshortener.repository.UserRepository
import com.rvladimir.urlshortener.service.UserService
import com.rvladimir.urlshortener.service.dto.UserDTO
import com.rvladimir.urlshortener.service.mapper.UserMapper
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class UserServiceImpl(
    private val userRepository: UserRepository,
    private val userMapper: UserMapper,
    private val passwordEncoder: PasswordEncoder
) : UserService {
    override fun create(userDTO: UserDTO): UserDTO {
        var user = userMapper.toEntity(userDTO)
        user.password = passwordEncoder.encode(user.password)
        user = userRepository.save(user)
        return userMapper.toDto(user)
    }

    @Transactional(readOnly = true)
    override fun findByEmail(email: String): User? {
        return userRepository.findByEmail(email)
    }

    @Transactional(readOnly = true)
    override fun findAll(): List<User> {
        return userRepository.findAll()
    }

    override fun loadUserByUsername(email: String): UserDetails {
        val user = userRepository.findByEmail(email)

        return org.springframework.security.core.userdetails.User(
            user?.email,
            user?.password,
            emptyList()
        )
    }
}
