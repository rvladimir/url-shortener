package com.rvladimir.urlshortener.service.impl

import com.rvladimir.urlshortener.repository.UrlRepository
import com.rvladimir.urlshortener.service.UrlService
import com.rvladimir.urlshortener.service.dto.UrlDTO
import com.rvladimir.urlshortener.service.mapper.UrlMapper
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class UrlServiceImpl(
    private val urlRepository: UrlRepository,
    private val urlMapper: UrlMapper
) : UrlService {

    override fun create(urlDTO: UrlDTO): UrlDTO {
        var url = urlMapper.toEntity(urlDTO)
        url = urlRepository.save(url)
        return urlMapper.toDto(url)
    }

    override fun findAll(pageable: Pageable): Page<UrlDTO> {
        return urlRepository.findAll(pageable).map(urlMapper::toDto)
    }
}
