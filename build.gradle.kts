import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    war
    id("org.springframework.boot") version "2.7.13"
    id("io.spring.dependency-management") version "1.0.15.RELEASE"
    kotlin("jvm") version "1.6.21"
    kotlin("plugin.spring") version "1.6.21"
    kotlin("plugin.jpa") version "1.6.21"
    id("org.jlleitschuh.gradle.ktlint") version "11.4.2"
    kotlin("kapt") version "1.6.21"
    id("jacoco")
}

group = "com.rvladimir"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

jacoco {
    toolVersion = "0.8.10"
    reportsDirectory.set(layout.buildDirectory.dir("jacocoReports"))
}

repositories {
    mavenCentral()
}

extra["testcontainersVersion"] = "1.18.3"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.jsonwebtoken:jjwt-api:0.11.5")
    implementation("io.jsonwebtoken:jjwt-impl:0.11.5")
    implementation("io.jsonwebtoken:jjwt-jackson:0.11.5")
    implementation("org.springdoc:springdoc-openapi-data-rest:1.6.15")
    implementation("org.springdoc:springdoc-openapi-ui:1.6.15")
    implementation("org.springdoc:springdoc-openapi-kotlin:1.6.15")
    implementation("org.mapstruct:mapstruct:1.5.3.Final")
    annotationProcessor("org.mapstruct:mapstruct-processor:1.5.3.Final")
    kapt("org.mapstruct:mapstruct-processor:1.5.3.Final")
    runtimeOnly("org.postgresql:postgresql")
    providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("org.testcontainers:testcontainers")
    testImplementation("org.testcontainers:junit-jupiter")
    testImplementation("org.testcontainers:postgresql")
}

dependencyManagement {
    imports {
        mavenBom("org.testcontainers:testcontainers-bom:${property("testcontainersVersion")}")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs += "-Xjsr305=strict"
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    classDirectories.setFrom(
        files(
            classDirectories.files.map {
                fileTree(it).apply {
                    setExcludes(
                        listOf(
                            "com/rvladimir/urlshortener/domain/**",
                            "com/rvladimir/urlshortener/service/mapper/**",
                            "com/rvladimir/urlshortener/config/**",
                            "com/rvladimir/urlshortener/service/dto/**",
                            "com/rvladimir/urlshortener/security/**",
                            "com/rvladimir/urlshortener/ServletInitializer*",
                            "com/rvladimir/urlshortener/UrlshortenerApplication*",
                            "com/rvladimir/urlshortener/UrlshortenerApplicationKt*"
                        )
                    )
                }
            }
        )
    )
}

tasks.jacocoTestCoverageVerification {
    classDirectories.setFrom(
        files(
            classDirectories.files.map {
                fileTree(it).apply {
                    setExcludes(
                        listOf(
                            "com/rvladimir/urlshortener/domain/**",
                            "com/rvladimir/urlshortener/service/mapper/**",
                            "com/rvladimir/urlshortener/config/**",
                            "com/rvladimir/urlshortener/service/dto/**",
                            "com/rvladimir/urlshortener/security/**",
                            "com/rvladimir/urlshortener/ServletInitializer*",
                            "com/rvladimir/urlshortener/UrlshortenerApplication*",
                            "com/rvladimir/urlshortener/UrlshortenerApplicationKt*"
                        )
                    )
                }
            }
        )
    )
    violationRules {
        rule {
            limit {
                counter = "INSTRUCTION"
                value = "COVEREDRATIO"
                minimum = "0.8".toBigDecimal()
            }
        }
        rule {
            limit {
                counter = "BRANCH"
                value = "COVEREDRATIO"
                minimum = "0.8".toBigDecimal()
            }
        }
    }
    dependsOn(tasks.jacocoTestReport)
}

tasks.register("installPreCommitGitHook", Copy::class) {
    from(file("$rootDir/pre-commit"))
    into(file("$rootDir/.git/hooks/"))
}

tasks.register("installPrePushGitHook", Copy::class) {
    from(file("$rootDir/pre-push"))
    into(file("$rootDir/.git/hooks/"))
}

afterEvaluate {
    tasks.getByPath("build").dependsOn("installPreCommitGitHook")
    tasks.getByPath("build").dependsOn("installPrePushGitHook")
}
